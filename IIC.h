#ifndef _IIC_H
#define _IIC_H

#include <reg52.h>

sbit SCL=P0^4;
sbit SDA=P0^5;

void At24c02_write(unsigned char addr,unsigned char dat);
unsigned char At24c02_read(unsigned char addr);

void dat_init();
void write_store();
void read_store();

#endif


