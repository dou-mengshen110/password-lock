#include "lcd12864.h"
#include "IIC.h"
#include "key.h"
#include "ds1302.h"

extern uchar bitnum,key,i,KeyVal,KeyBuf[6];
extern bit flag;
uchar code table1[]="Enter Password";//14
uchar code table2[]="Error!";//6
uchar code table3[]="Open!";//5
uchar code table4[]="New Password";//12
uchar code table5[]=":";//1
uchar code table6[]="-";//1
//LCD显示0，1，2，3，4，5，6，7，8，9
extern uchar code LCD_disp[]=
{0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39};

/*********************DS1302**************************************************/
uchar disp[8];
uchar disp1[11];

uchar Write_DS1302_addr[7]=
{0x80,0x82,0x84,0x86,0x88,0x8a,0x8c};

uchar Read_DS1302_addr[7]=
{0x81,0x83,0x85,0x87,0x89,0x8b,0x8d};

uchar Timer[7]=
//2021年4月06日 12：16：00  周二
{0x00,0x16,0x12,0x06,0x04,0x02,0x21};
/*****************************************************************************/

/*******************************************************************************
* 函 数 名         : LCD12864_Delay1ms
* 函数功能		   : 延时1MS
* 输    入         : c
* 输    出         : 无
*******************************************************************************/

void LCD12864_Delay1ms(uint c)
{
    uchar a,b;
	for(; c>0; c--)
	{
	    for(b=199; b>0; b--)
		{
	        for(a=1; a>0; a--);
		}
	}
}

/*******************************************************************************
* 函 数 名         : LCD12864_Busy
* 函数功能		   : 检测LCD是否忙
* 输    入         : 无
* 输    出         : 1或0（1表示不忙，0表示忙）
*******************************************************************************/

uchar LCD12864_Busy(void)
{
	uchar i = 0;

	LCD12864_RS = 0;   	//选择命令
	LCD12864_RW = 1;	//选择读取

	LCD12864_EN = 1;
	LCD12864_Delay1ms(1);

	while((LCD12864_DATAPORT & 0x80) == 0x80)	//检测读取到的值
	{
		i++;
		if(i > 100)
		{
			LCD12864_EN = 0;
			return 0;	   //超过等待时间返回0表示失败
		}
	}

	LCD12864_EN = 0;

	return 1;
}

/*******************************************************************************
* 函 数 名         : LCD12864_WriteCmd
* 函数功能		   : 写命令
* 输    入         : cmd
* 输    出         : 无
*******************************************************************************/

void LCD12864_WriteCmd(uchar cmd)
{
	uchar i;
	i = 0;
	while( LCD12864_Busy() == 0)
	{
		LCD12864_Delay1ms(1);
		i++;
		if( i>100)
		{
			return;	   //超过等待退出
		}	
	}
	
	LCD12864_RS = 0;     //选择命令
	LCD12864_RW = 0;     //选择写入
	LCD12864_EN = 0;     //初始化使能端

	LCD12864_DATAPORT = cmd;   //放置数据

	LCD12864_EN = 1;		   //写时序
	LCD12864_Delay1ms(5);
	LCD12864_EN = 0;    					
}

/*******************************************************************************
* 函 数 名         : LCD12864_WriteData
* 函数功能		   : 写数据
* 输    入         : dat
* 输    出         : 无
*******************************************************************************/

void LCD12864_WriteData(uchar dat)
{
	uchar i;
	i = 0;
	while( LCD12864_Busy() == 0)
	{
		LCD12864_Delay1ms(5);
		i++;
		if( i>100)
		{
			return;	   //超过等待退出
		}	
	}

	LCD12864_RS = 1;     //选择数据
	LCD12864_RW = 0;     //选择写入
	LCD12864_EN = 0;     //初始化使能端

	LCD12864_DATAPORT = dat;   //放置数据

	LCD12864_EN = 1;		   //写时序
	LCD12864_Delay1ms(5);
	LCD12864_EN = 0;    								
}



/*******************************************************************************
* 函 数 名         : LCD12864_Init
* 函数功能		   : 初始化LCD12864
* 输    入         : 无
* 输    出         : 无
*******************************************************************************/

void LCD12864_Init()
{
	LCD12864_PSB = 1;	  //选择并行输入
	LCD12864_RST = 1;	  //复位

	LCD12864_WriteCmd(0x30);  //选择基本指令操作
	LCD12864_WriteCmd(0x0c);  //显示开，关光标
	LCD12864_WriteCmd(0x01);  //清除LCD12864的显示内容
}


/*******************************************************************************
* 函 数 名         : LCD12864_SetWindow
* 函数功能		   : 设置在基本指令模式下设置显示坐标，并显示汉字。注意：x是设置行，y是设置列
* 输    入         : x, y，word
* 输    出         : 无
*******************************************************************************/

void LCD12864_SetWindow(uchar x, uchar y,uchar *word)
{
	uchar pos;
	uchar i=y;
	if(x == 0)	   // 第一行的地址是80H
	{
		x = 0x80;
	}
		
	else if(x == 1)  //第二行的地址是90H
	{
		x = 0x90;	
	}
	
	else if(x == 2)  //第三行的地址是88H
	{
		x = 0x88;
	}
	
	else if(x == 3)  //第四行的地址是98H
	{
		x = 0x98;
	}
	
	pos = x + y;
	LCD12864_WriteCmd(pos);
	
	while(word[i]!='\0')
	{
		if(i<16)    //判断是否在本行的显示范围内
		{
			LCD12864_WriteData(word[i]);
			i++;
		}
		
	 }
}

/***************************DS1302***************************/
/************************************************************/

/****************时间初始化*********************/
//void Write_Config()
//{
//	uchar i;
//	Write_Ds1302_Byte(0x8e,0x00);
//	for(i=0;i<7;i++)
//	{
//		Write_Ds1302_Byte(Write_DS1302_addr[i],Timer[i]);
//	}
//	Write_Ds1302_Byte(0x8e,0x80);
//}

/**********************************************/

void Read_DS1302_Timer()
{
	uchar u;
	for(u=0;u<7;u++)
	{
		Timer[u]=Read_Ds1302_Byte(Read_DS1302_addr[u]);
	}
}

void datapors()
{
	/*时间*/
	disp[0]=LCD_disp[Timer[2]/16];
	disp[1]=LCD_disp[Timer[2]%16];
	disp[2]=table5[0];
	disp[3]=LCD_disp[Timer[1]/16];
	disp[4]=LCD_disp[Timer[1]%16];
	disp[5]=table5[0];;
	disp[6]=LCD_disp[Timer[0]/16];
	disp[7]=LCD_disp[Timer[0]%16];
	
	/*年月日*/
	disp1[0]=0x32;
	disp1[1]=0x30;
	disp1[2]=LCD_disp[Timer[6]/16];//年
	disp1[3]=LCD_disp[Timer[6]%16];
	disp1[4]=table6[0];
	disp1[5]=LCD_disp[Timer[4]/16];//月
	disp1[6]=LCD_disp[Timer[4]%16];
	disp1[7]=table6[0];
	disp1[8]=LCD_disp[Timer[3]/16];//日
	disp1[9]=LCD_disp[Timer[3]%16];
}
/************************************************************/
/************************************************************/


/**************************************************************
***************************************************************
***************************************************************/
/***************************屏幕显示***************************/
void Display()
{
	if(key==0)
		{
			LCD12864_WriteCmd(0x80);
			for(i=0;i<10;i++)
			LCD12864_WriteData(disp1[i]);
			LCD12864_WriteCmd(0x90);
			for(i=0;i<8;i++)
			LCD12864_WriteData(disp[i]);
			LCD12864_WriteCmd(0x88);
			for(i=0;i<14;i++)
			LCD12864_WriteData(table1[i]);
			LCD12864_WriteCmd(0x98);
			for(i=0;i<bitnum;i++)
			LCD12864_WriteData(0x2a);//"*"
		}
	
	if(key==1)//密码错误
		{LCD12864_WriteCmd(0x80);
			for(i=0;i<10;i++)
			LCD12864_WriteData(disp1[i]);
			LCD12864_WriteCmd(0x90);
			for(i=0;i<8;i++)
			LCD12864_WriteData(disp[i]);
			LCD12864_WriteCmd(0x88);
			for(i=0;i<14;i++)
			LCD12864_WriteData(table1[i]);
			LCD12864_WriteCmd(0x98);
			for(i=0;i<6;i++)
			LCD12864_WriteData(table2[i]);	
			KeyVal=16;
		}		
	if(key==2)//密码正确
		{
			LCD12864_WriteCmd(0x80);
			for(i=0;i<10;i++)
			LCD12864_WriteData(disp1[i]);
			LCD12864_WriteCmd(0x90);
			for(i=0;i<8;i++)
			LCD12864_WriteData(disp[i]);
			LCD12864_WriteCmd(0x88);
			for(i=0;i<14;i++)
			LCD12864_WriteData(table1[i]);
			LCD12864_WriteCmd(0x98);
			for(i=0;i<6;i++)
			LCD12864_WriteData(table3[i]);		
			KeyVal=16;
		}		
	while(key==3)
	{
		key_Scan();
	if(KeyVal>=0&&KeyVal<10&&bitnum<6)
	{
		KeyBuf[bitnum]=KeyVal;
		if(flag==1)
		{
			flag=0;
			bitnum++;		
		}
		KeyVal=16;
	}
	if(KeyVal==10)
	{
		bitnum=0;
		flag=0;
		KeyVal=16;
		LCD12864_WriteCmd(0x01);
		LED=1;
	}
	if(KeyVal==14)
	{
		if(bitnum==6)
		{
			write_store();
			bitnum=0;
			flag=0;
			KeyVal=16;
			LCD12864_WriteCmd(0x01);
			key=0;
		}
	}
		LCD12864_WriteCmd(0x80);
		for(i=0;i<10;i++)
		LCD12864_WriteData(disp1[i]);
		LCD12864_WriteCmd(0x90);
		for(i=0;i<8;i++)
		LCD12864_WriteData(disp[i]);
		LCD12864_WriteCmd(0x88);
		for(i=0;i<12;i++)
		LCD12864_WriteData(table4[i]);
		LCD12864_WriteCmd(0x98);
		for(i=0;i<bitnum;i++)
		LCD12864_WriteData(LCD_disp[KeyBuf[i]]);
	}
}

