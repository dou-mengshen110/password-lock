#include "key.h"
#include "IIC.h"
#include "lcd12864.h"	
#include <string.h>

extern uchar temp,KeyVal,bitnum,key,KeyBuf[6],KeyBuf2[6];
extern bit flag;
uchar temp,error = 0;

void delayms(uint i)
{
	uchar j;
	while(i--)
		for(j=0;j<88;j++);
}

void dianji_start()
{
	IN0 = 0;
	delayms(50);
	IN0 = 1;
}

void dianji_stop()
{
	IN0 = 0;
	IN1 = 1;
}

void dianji()
{
	dianji_start();
	delayms(500);
	dianji_stop();
}

void key_Scan(void)
{
	P2=0xfe;
	temp=P2;
	temp=temp&0xf0;
	if(temp!=0xf0)
	{
		delayms(10);
		temp=P2;
		temp=temp&0xf0;
		if(temp!=0xf0)
		{
			temp=P2;
			switch(temp)
			{
				case 0xee:KeyVal=1;flag=1;
				break;
				case 0xde:KeyVal=2;flag=1;
				break;
				case 0xbe:KeyVal=3;flag=1;
				break;
				case 0x7e:KeyVal=12;flag=1;
				break;
			}
			while(temp!=0xf0)
			{
				temp=P2;
				temp=temp&0xf0;
			}
		}
	}
	
	P2=0xfd;
	temp=P2;
	temp=temp&0xf0;
	if(temp!=0xf0)
	{
		delayms(10);
		temp=P2;
		temp=temp&0xf0;
		if(temp!=0xf0)
		{
			temp=P2;
			switch(temp)
			{
				case 0xed:KeyVal=4;flag=1;
				break;
				case 0xdd:KeyVal=5;flag=1;
				break;
				case 0xbd:KeyVal=6;flag=1;
				break;
				case 0x7d:KeyVal=13;flag=1;
				break;
			}
			while(temp!=0xf0)
			{
				temp=P2;
				temp=temp&0xf0;
			}
		}
	}
	
	P2=0xfb;
	temp=P2;
	temp=temp&0xf0;
	if(temp!=0xf0)
	{
		delayms(10);
		temp=P2;
		temp=temp&0xf0;
		if(temp!=0xf0)
		{
			temp=P2;
			switch(temp)
			{
				case 0xeb:KeyVal=7;flag=1;
				break;
				case 0xdb:KeyVal=8;flag=1;
				break;
				case 0xbb:KeyVal=9;flag=1;
				break;
				case 0x7b:KeyVal=14;flag=1;
				break;
			}
			while(temp!=0xf0)
			{
				temp=P2;
				temp=temp&0xf0;
			}
		}
	}
	
	P2=0xf7;
	temp=P2;
	temp=temp&0xf0;
	if(temp!=0xf0)
	{
		delayms(10);
		temp=P2;
		temp=temp&0xf0;
		if(temp!=0xf0)
		{
			temp=P2;
			switch(temp)
			{
				case 0xe7:KeyVal=10;flag=1;
				break;
				case 0xd7:KeyVal=0;flag=1;
				break;
				case 0xb7:KeyVal=11;flag=1;
				break;
				case 0x77:KeyVal=15;flag=1;	
				break;
			}
			while(temp!=0xf0)
			{
				temp=P2;
				temp=temp&0xf0;
			}
		}
	}
}

/**********按键数据处理子程序***********/
void data_hand()
{
	key_Scan();
	if(KeyVal>=0&&KeyVal<10&&bitnum<6)
	{
		KeyBuf[bitnum]=KeyVal;
		if(flag==1)
		{
			flag=0;
			bitnum++;		
		}
		KeyVal=16;
	}
}
/*************按键功能处理***************/
void key_cmd()
{
		if(KeyVal==11&&bitnum==6)
		{
			read_store();
			if(strcmp(KeyBuf,KeyBuf2)!=0)
			{
				key=1;//密码错误
				error++;
				//LED=1;//灭
				KeyVal=16;
			}
		 if(strcmp(KeyBuf,KeyBuf2)==0)
			{	
				key=2;//密码正确
				dianji();
				//LED=0;//亮
				KeyVal=16;
			}	
		}
		if(KeyVal==10)
		{
			dat_init();
			LCD12864_WriteCmd(0x01);//清屏
			//LED=1;
			flag=0;
		}
		if(KeyVal==12)
		{
			dat_init();
			LCD12864_WriteCmd(0x01);
			LED=1;
			flag=0;
		}
		if(KeyVal==13)
		{
			if(key==2)
			{
			dat_init();
			LCD12864_WriteCmd(0x01);	
			LED=1;
			key=3;
			}
		}
		if(error == 3)
		{
			error=0;
			LED=0;
		}
}
