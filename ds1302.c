/*
  程序说明: DS1302驱动程序
  软件环境: Keil uVision 4.10 
  硬件环境: CT107单片机综合实训平台 8051，12MHz
  日    期: 2011-8-9
*/

#include <reg52.h>
#include <intrins.h>

sbit SCK1=P0^1;		
sbit SDA1=P0^2;		
sbit RST = P0^3;   // DS1302复位												

void Write_Ds1302(unsigned  char temp) 
{
	unsigned char i;
	for (i=0;i<8;i++)     	
	{ 
		SCK1=0;
		SDA1=temp&0x01;
		temp>>=1; 
		SCK1=1;
	}
} 


////写入初试时间用的
//void Write_Ds1302_Byte( unsigned char address,unsigned char dat )     
//{
// 	RST=0;	_nop_();
// 	SCK1=0;	_nop_();
// 	RST=1; 	_nop_();  
// 	Write_Ds1302(address);	
// 	Write_Ds1302(dat);		
// 	RST=0; 
//}

unsigned char Read_Ds1302_Byte ( unsigned char address )
{
 	unsigned char i,temp=0x00;
 	RST=0;	_nop_();
 	SCK1=0;	_nop_();
 	RST=1;	_nop_();
 	Write_Ds1302(address);
 	for (i=0;i<8;i++) 	
 	{		
		SCK1=0;
		temp>>=1;	
 		if(SDA1)
 		temp|=0x80;	
 		SCK1=1;
	} 
 	RST=0;	_nop_();
 	SCK1=0;	_nop_();
	SCK1=1;	_nop_();
	SDA1=0;	_nop_();
	SDA1=1;	_nop_();
	return (temp);			
}
