#include "IIC.h"
#include "key.h"

#define uchar unsigned char

uchar i,num1,num2,bitnum,key,KeyVal,KeyBuf[6],KeyBuf2[6];
bit flag;

void delay10us(unsigned char time)
{
	while(time--);
}

void IIC_start()
{
	SDA = 1;
	SCL = 1;
	delay10us(5);
	SDA = 0;        //在SCL高电平期间，SDA由高变低
	delay10us(5);
	SCL = 0;	

}

void IIC_stop()
{
	SDA = 0;
	SCL = 1;
	delay10us(5);
	SDA = 1;	//在SCL高电平期间，SDA由高变低
	delay10us(5);

}

void IIC_Ack(unsigned char ackbit)
{
	if(ackbit) 	
		SDA = 0;	//产生应答信号
	else 
		SDA = 1;	//产生非应答信号
	delay10us(5);
	SCL = 1;
	delay10us(5);	        //第9个时钟周期
        SCL = 0;
	SDA = 1; 		//释放SDA线
	delay10us(5);
}

bit IIC_WaitAck(void)
{
	SDA = 1;
	delay10us(5);
	SCL = 1;
	delay10us(5);	
 
	if(SDA)    		//在SCL高电平期间，SDA为高电平，从机非应答。
	{   
		SCL = 0;
		IIC_stop();
		return 0;
	}
	else 			//在SCL高电平期间，SDA为低电平，从机有应答。
	{ 
		SCL = 0;
		return 1;
	}	
}

void IIC_SendByte(unsigned char byt)//发送字节+应答
{
	unsigned char i;
	for(i=0;i<8;i++)		//循环发送8位数据
	{   
		if(byt & 0x80) 		//数据位是高电平
		{	
			SDA = 1;
		}
		else 			//数据位是低电平
		{
			SDA = 0;
		}
 
		delay10us(5);
		SCL = 1;		//SCL高电平期间，SDA的数据要保持稳定
		byt <<= 1;		//发送的数据左移，准备发送下一位
		delay10us(5);		//等待SDA的数据被读取
		SCL = 0;
	}
}

unsigned char IIC_ReadByte()
{
	unsigned char dat;
	unsigned char i;
	for(i=0;i<8;i++)
	{   
		SCL = 1;
		delay10us(5);		//在SCL高电平期间，读取SDA的数据
		dat <<= 1;
		if(SDA) 
			dat |= 0x01;
		SCL = 0;
		delay10us(5);
	}
	return dat;
}


void At24c02_write(unsigned char addr,unsigned char dat)
{
	IIC_start();
	IIC_SendByte(0xa0);
	IIC_WaitAck();
	IIC_SendByte(addr);//At24c20写芯片内部地址。0~255
	IIC_WaitAck();	
	IIC_SendByte(dat);
	IIC_WaitAck();	
	IIC_stop();
}

unsigned char At24c02_read(unsigned char addr)
{
	unsigned char num;
	IIC_start();
	IIC_SendByte(0xa0);
	IIC_WaitAck();	
	IIC_SendByte(addr);
	IIC_WaitAck();	
	
	IIC_start();
	IIC_SendByte(0xa1);
	IIC_WaitAck();	
	num=IIC_ReadByte();
	IIC_Ack(0);
	IIC_stop();
	return num;
} 

/**************************************************************/
/********************初始化数据，读写EEPROM*********************/
void dat_init()
{
	bitnum=0;
	flag=0;
	KeyVal=16;
	key=0;
}

void write_store()
{
	for(i=0;i<6;i++)
	{
		num1 = KeyBuf[i];
		At24c02_write(i+6,num1);
		delayms(200);
	}
}

void read_store()
{
	for(i=0;i<6;i++)
	{
		num2 = At24c02_read(i+6);
		KeyBuf2[i] = num2;
	}
}
/*********************************/
	
