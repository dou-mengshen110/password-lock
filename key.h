#ifndef __KEY_H
#define __KEY_H

#include <reg52.h>


#define uint unsigned int
#define uchar unsigned char
sbit LED=P0^6;

sbit IN0 = P4^4;
sbit IN1 = P4^5;

void key_Scan(void);
void delayms(uint i);
void data_hand();
void key_cmd();

#endif